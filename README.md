Ansible Archlinux Piwigo
========================

A role to install piwigo and set basic config

It also install apache, ssl cert, php-fpm

Tested and Used on ArchLinux but it may work on any Linux

Dependencies
------------

[Requirements](molecule/default/requirements.yml)


Variables
---------

[Role Variables](defaults/main.yml)


Example Playbook
----------------

[Test Playbook](molecule/default/converge.yml)


License
-------

[License](LICENSE)

How To Test
-----------

You must have Ansible, Molecule and Podman installed on your Archlinux Host.

Run:

``` shell
mkdir -p Air/roles
cd Air/roles

# clone repo
git clone https://git.laquadrature.net/sympathisant-es/archlinux-install-on-remote/air-ansible/air.piwigo.git

# start piwigo in molecule
cd air.piwigo
molecule  test --destroy=never

```

Open Firefox and Goto https://localhost:18443/

Accept Self Signed Certificat

Todo
----
- Be Kind
